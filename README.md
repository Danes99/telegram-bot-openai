# QuizGPT

## Description

This project is a [Telegram Bot](https://core.telegram.org/bots/) generating quiz using [OpenAI API](https://platform.openai.com/docs/introduction) and posting them to Telegram Channels.

The bot queries the OpenAI API to generate a certification (AWS, Azure, Kubernetes) quiz:

- a question
- 4 answers
- the ID of the correct answer

The prompt engineering is written so that, instead of returning a segment of text (like what ChatGPT would return) the OpenAI API return the content as a JSON File.

```json
{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}
```

The result can be parsed, just like an HTTP response from a [REST API](https://en.wikipedia.org/wiki/Representational_state_transfer).
Then the JSON is passed to the Telegram SDK to post a quiz in a Telegram Channel.

### Tech Stack

The Bot is written in [TypeScript](https://www.typescriptlang.org/).
It uses the [Telegram SDK (telegraf)](https://www.npmjs.com/package/telegraf) and the [OpenAI SDK](https://www.npmjs.com/package/openai).

## Installation

Clone this repository and then run:

```sh
npm install ci
npm run build
```

## Usage

Create the following environment variables:

```env
# OpenAI: Credentials
OPENAI_ORGANIZATION_ID="my_openai_organization_id"
OPENAI_API_KEY="my_openai_api_key"

# OpenAI: Config
OPENAI_MODEL_TEMPERATURE=1

# Telegram: Credentials
TELEGRAM_CHANNEL_ID="@my_telegram_channel_id"
TELEGRAM_BOT_ACCESS_TOKEN="my_telegram_bot_access_token"
```

```sh
npm run start
```

## Support

If you need help, please go to the [project issue tracker](https://gitlab.com/Danes99/telegram-bot-openai/-/issues).

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

You can also document commands to [lint](https://stackoverflow.com/questions/8503559/what-is-linting) the code or run [tests](https://en.wikipedia.org/wiki/Test_automation).These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something.

## Authors and acknowledgment

[Danes99](https://gitlab.com/Danes99)

## License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)

## Project status

Currently working on the project.
