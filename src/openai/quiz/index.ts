import { Configuration, OpenAIApi } from "openai";
import type {
  ChatCompletionRequestMessage,
  CreateChatCompletionRequest,
} from "openai";

const configuration = new Configuration({
  organization: process.env.OPENAI_ORGANIZATION_ID,
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

const GPT_MODEL = "gpt-4";
// const GPT_MODEL = "text-davinci-003";
// const GPT_MODEL = "gpt-3.5-turbo-0301";

const MAX_TOKENS = 4097;
const TEMPERATURE = parseFloat(process.env.OPENAI_MODEL_TEMPERATURE || "1");

interface IQuiz {
  question: string;
  options: [string];
  correct_option_index: number;
}

export const getTelegramQuiz = async (
  prompt: string
): Promise<IQuiz | undefined> => {
  try {
    const message: ChatCompletionRequestMessage = {
      role: "system",
      content: prompt,
    };

    const max_tokens = MAX_TOKENS - prompt.length;

    const chatCompletionRequestInput: CreateChatCompletionRequest = {
      model: GPT_MODEL,
      messages: [message],
      max_tokens,
      temperature: TEMPERATURE,
    };

    const chatCompletion = await openai.createChatCompletion(
      chatCompletionRequestInput
    );
    const answer = chatCompletion.data.choices[0].message?.content as string;

    return JSON.parse(answer) as IQuiz;
  } catch (error: any) {
    if (error.response) {
      console.log(error.response.status);
      console.log(error.response.data);
    } else {
      console.log(error.message);
    }

    return undefined;
  }
};
