export const PROMPT_MICROSOFT_AZ_900 = `
I want to write a practice test for the Exam Microsoft AZ-900.
Write a question about Azure Cloud with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_MICROSOFT_AZ_300 = `
I want to write a practice test for the Exam Microsoft AZ-300.
Write a question about Azure Cloud with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;
