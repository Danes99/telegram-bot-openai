export const PROMPT_AWS_SOLUTION_ARCHITECT_ASSOCIATE = `
I want to write a practice test for the Exam AWS Solution Architect Associate.
Write a question about AWS Cloud with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_AWS_SOLUTION_ARCHITECT_PROFESSIONAL = `
I want to write a practice test for the Exam AWS Solution Architect Professional.
Write a question about AWS Cloud Architecture with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_AWS_SYS_OPS_ADMINISTRATOR = `
I want to write a practice test for the Exam AWS SysOps Administrator Associate.
Write a question about AWS Cloud administration with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_AWS_WELL_ARCHITECTED_FRAMEWORK = `
I want to write a practice test for the Exam AWS Solution Architect Professional.
Write a question about AWS Well Architected Framework with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_AWS_SECURITY = `
I want to write a practice test for the Exam AWS Security Specialist.
Write a question about AWS Security with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;
