export const PROMPT_KUBERNETES_ADMINISTRATOR = `
I want to write a practice test for the Exam Certified Kubernetes Administrator.
Write a question about Kubernetes with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_KUBERNETES_APP = `
I want to write a practice test for the Exam Certified Kubernetes Application developer.
Write a question about Kubernetes Application Development with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_KUBERNETES_SECURITY = `
I want to write a practice test for the Exam Certified Kubernetes Application developer.
Write a question about Kubernetes Security with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;

export const PROMPT_KUBERNETES_NETWORKING = `
I want to write a practice test for the Exam Certified Kubernetes Application developer.
Write a question about Kubernetes Networking with 4 choice answer. 
Tell me which answer is the good one.
Print it in the JSON format:

{
  "question": "My question",
  "options": ["Choice 1", "Choice 2", "Choice 3", "Choice 4"],
  "correct_option_index": 1
}

correct_option_index: min value 0, max value length of "options" array - minus 1
Print only the JSON file.`;
