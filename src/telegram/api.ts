const BASE_URI = "https://api.telegram.org/";

const BOT_ENDPOINT =
  BASE_URI + "bot" + process.env.TELEGRAM_BOT_ACCESS_TOKEN + "/";

const METHOD_SEND_POLL = BOT_ENDPOINT + "sendPoll";

export interface IPoll {
  chat_id: string;
  question: string;
  options: [string];
  correct_option_id: number;
}

export const sendQuiz = async (poll: IPoll): Promise<object> => {
  try {
    const telegramPoll = {
      ...poll,
      type: "quiz",
    };

    const response = await fetch(METHOD_SEND_POLL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(telegramPoll),
    });

    const body = await response.json();

    if (!response.ok) {
      console.error(response.status, await response.json());
      return {};
    }

    return body;
  } catch (error: any) {
    console.error(error.message);
    return {};
  }
};
