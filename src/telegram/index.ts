// Import downloaded module
import { Telegraf } from "telegraf";

// Import downloaded type
import type { Message } from "telegraf/typings/core/types/typegram";

// Import custom functions
import { getTelegramQuiz } from "../openai/quiz";

import {
  PROMPT_MICROSOFT_AZ_300,
  PROMPT_AWS_SOLUTION_ARCHITECT_PROFESSIONAL,
  PROMPT_KUBERNETES_ADMINISTRATOR,
  PROMPT_KUBERNETES_APP,
  PROMPT_KUBERNETES_SECURITY,
} from "../openai/prompt";

const bot = new Telegraf(process.env.TELEGRAM_BOT_ACCESS_TOKEN || "");

const arrayOfPrompt = [
  PROMPT_MICROSOFT_AZ_300,
  PROMPT_AWS_SOLUTION_ARCHITECT_PROFESSIONAL,
  PROMPT_KUBERNETES_ADMINISTRATOR,
  PROMPT_KUBERNETES_APP,
  PROMPT_KUBERNETES_SECURITY,
];

const TELEGRAM_CHANNEL_ID =
  process.env.TELEGRAM_CHANNEL_ID || "@test_az900_channel";

export interface IPublishQuizResult {
  ok: boolean;
  body?: Message.PollMessage;
  error?: string;
}

export const publishQuiz = async (
  prompt: string
): Promise<IPublishQuizResult> => {
  // Generate quiz
  const quiz = await getTelegramQuiz(prompt);

  if (quiz) {
    console.info(quiz.question);

    const result = await bot.telegram.sendQuiz(
      TELEGRAM_CHANNEL_ID,
      quiz.question,
      quiz.options,
      {
        correct_option_id: quiz.correct_option_index,
      }
    );

    return {
      ok: true,
      body: result,
    };
  }

  return {
    ok: false,
    error: "No quiz",
  };
};

export async function main(args: object): Promise<object> {
  const arrayOfPromises: Promise<object>[] = arrayOfPrompt.map(
    async (element) => publishQuiz(element)
  );

  await Promise.all(arrayOfPromises);

  return args;
}
