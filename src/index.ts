// Import downloaded module
import { Telegraf } from "telegraf";

// Import downloaded type
import type { Message } from "telegraf/typings/core/types/typegram";

// Import custom functions
import { getTelegramQuiz } from "./openai/quiz";

import {
  PROMPT_MICROSOFT_AZ_300,
  PROMPT_AWS_SOLUTION_ARCHITECT_PROFESSIONAL,
  PROMPT_AWS_SECURITY,
  PROMPT_AWS_SYS_OPS_ADMINISTRATOR,
  PROMPT_KUBERNETES_ADMINISTRATOR,
  PROMPT_KUBERNETES_SECURITY,
  PROMPT_KUBERNETES_NETWORKING,
} from "./openai/prompt";

const ARRAY_OF_PROMPT_DEFAULT = [PROMPT_MICROSOFT_AZ_300];

const ARRAY_OF_PROMPT_KUBERNETES = [
  PROMPT_KUBERNETES_ADMINISTRATOR,
  PROMPT_KUBERNETES_NETWORKING,
  PROMPT_KUBERNETES_SECURITY,
];

const ARRAY_OF_PROMPT_AWS = [
  PROMPT_AWS_SOLUTION_ARCHITECT_PROFESSIONAL,
  PROMPT_AWS_SECURITY,
  PROMPT_AWS_SYS_OPS_ADMINISTRATOR,
];

export interface IPublishQuizResult {
  ok: boolean;
  body?: Message.PollMessage;
  error?: string;
}

const bot = new Telegraf(process.env.TELEGRAM_BOT_ACCESS_TOKEN || "");

export const publishQuiz = async (
  channelID: string,
  prompt: string
): Promise<IPublishQuizResult> => {
  // Generate quiz
  const quiz = await getTelegramQuiz(prompt);

  if (quiz) {
    console.info(quiz.question);

    const result = await bot.telegram.sendQuiz(
      channelID,
      quiz.question,
      quiz.options,
      {
        correct_option_id: quiz.correct_option_index,
      }
    );

    return {
      ok: true,
      body: result,
    };
  }

  return {
    ok: false,
    error: "No quiz",
  };
};

export const publishQuizList = async (
  channelID: string,
  prompts: string[]
): Promise<IPublishQuizResult[]> => {
  const arrayOfPromises: Promise<IPublishQuizResult>[] = prompts.map(
    async (element: string) => publishQuiz(channelID, element)
  );

  const results = await Promise.all(arrayOfPromises);

  return results;
};

export async function main(args: object): Promise<object> {
  const channelID = process.env.TELEGRAM_CHANNEL_ID;

  switch (channelID) {
    // Kubernetes
    case "@quiz_gpt_kubernetes":
      await publishQuizList(channelID, ARRAY_OF_PROMPT_KUBERNETES);
      break;
    // AWS
    case "@quiz_gpt_aws":
      await publishQuizList(channelID, ARRAY_OF_PROMPT_AWS);
      break;
    // Default
    default:
      await publishQuizList("@test_az900_channel", ARRAY_OF_PROMPT_DEFAULT);
      break;
  }

  return args;
}
